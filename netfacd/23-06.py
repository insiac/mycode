#!/usr/bin/python3


import netifaces

print(netifaces.interfaces())

for i in netifaces.interfaces():
        print('**********' + "Details of int " + i + '**********' )
        try: 
            print(netifaces.ifaddresses(i)[netifaces.AF_LINK][0]['addr'])
            print(netifaces.ifaddresses(i)[netifaces.AF_INET][0]['addr'])
            print('\n')
        except:
            print('Could not collect adapter information for int:'+ i )

