#!/usr/bin/python3


import netifaces

print(netifaces.interfaces())

def getmac(iface):
    return netifaces.ifaddresses(iface)[netifaces.AF_LINK][0]['addr']

def getip(iface):
    return netifaces.ifaddresses(iface)[netifaces.AF_INET][0]['addr']


for i in netifaces.interfaces():
        print('**********' + "Details of int " + i + '**********' )
        try: 
            print('MAC: ', end='')
            print(getmac(i))
            print('IP: ', end='')
            print(getip(i))
            print('\n')
        except:
            print('Could not collect adapter information for int:'+ i )

