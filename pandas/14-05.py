#!/usr/bin/python3
"""Driving a simple game framework with
   a dictionary object | Alta3 Research"""

import json
import yaml

# Replace RPG starter project with this code when new instructions are live

def showInstructions():
    """Show the game instructions when called"""
    #print a main menu and the commands
    print('''
    RPG Game
    ========
    Commands:
      go [direction]
      get [item]

    Win: Get to Garden with key and potion.
    Loose: Get eaten by monster.
    ''')

def showStatus():
    """determine the current status of the player"""
    #print the player's current status
    print('---------------------------')
    print('You are in the ' + currentRoom)
    #print the current inventory
    print('Inventory : ' + str(inventory))
    #print an item if there is one
    if "item" in rooms[currentRoom]:
      print('You see a ' + rooms[currentRoom]['item'])
    print("---------------------------")


#an inventory, which is initially empty
inventory = []

#a dictionary linking a room to other rooms
"""
rooms = {

            'Hall' : {
                  'south' : 'Kitchen',
                  'east' : 'Dining Room',
                  'west' : 'Bath',
                  'item' : 'key'

                },
            'Bath' : {
                  'south' : 'Closet',
                  'east' : 'Hall',

                },
            'Closet' : {
                  'north' : 'Bath',
                  'east' : 'Kitchen',

                },

            'Kitchen' : {
                  'north' : 'Hall',
                  'west' : 'Closet',
                  'item' : 'monster'
                },

            'Dining Room' : {
                  'west' : 'Hall',
                  'south' : 'Garden',
                  'item' : 'potion'
                },
            'Garden' : {
                  'north' : 'Dining Room',
                }


         }
"""


def tofile():
    with open("game.json", "w") as jfile:
            ## use the JSON library
            ## USAGE: json.dump(input data, file like object) ##
            json.dump(rooms, jfile)

    with open("game.yaml", "w") as yfile:
        ## use the YAML library
        ## USAGE: yaml.dump(input data, file like object)
            yaml.dump(rooms, yfile)


def fromjson():
    with open("game.json", "r") as jfile:
            rooms = json.load(jfile)
            return rooms

def fromyaml():
    with open("game.yaml", "r") as yfile:
            rooms = yaml.load(yfile, Loader=yaml.FullLoader)
            return rooms

#start the player in the Hall
currentRoom = 'Hall'

showInstructions()

#loop forever
while True:
#    tofile()
 #   rooms = fromjson()
    rooms = fromyaml()
   # quit()
    showStatus()

    #get the player's next 'move'
    #.split() breaks it up into an list array
    #eg typing 'go east' would give the list:
    #['go','east']
    move = ''
    while move == '':  
        move = input('>')

    # split allows an items to have a space on them
    # get golden key is returned ["get", "golden key"]          
    move = move.lower().split(" ", 1)

    #if they type 'go' first
    if move[0] == 'go':
        #check that they are allowed wherever they want to go
        if move[1] in rooms[currentRoom]:
            #set the current room to the new room
            currentRoom = rooms[currentRoom][move[1]]
        #there is no door (link) to the new room
        else:
            print('You can\'t go that way!')

    #if they type 'get' first
    if move[0] == 'get' :
        #if the room contains an item, and the item is the one they want to get
        if "item" in rooms[currentRoom] and move[1] in rooms[currentRoom]['item']:
            #add the item to their inventory
            inventory += [move[1]]
            #display a helpful message
            print(move[1] + ' got!')
            #delete the item from the room
            del rooms[currentRoom]['item']
        #otherwise, if the item isn't there to get
        else:
            #tell them they can't get it
            print('Can\'t get ' + move[1] + '!')

    if 'item' in rooms[currentRoom] and 'monster' in rooms[currentRoom]['item']:
        print('A monster has got you... GAME OVER!')
        break

    if currentRoom == 'Garden' and  'key' in inventory and 'potion' in inventory:
        print('You escaped the house with the ultra rare key and magic potion... YOU WIN!')
        break
